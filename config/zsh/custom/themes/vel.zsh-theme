# Vel ZSH Theme
local return_code="%(?..%{$fg[red]%}%? ↵%{$reset_color%})"
local user_symbol='$'
local current_dir='%{$terminfo[bold]$fg[blue]%}$(shrink_path -f) %{$reset_color%}'
local git_branch='$(git_prompt_info)'

PROMPT="${current_dir}${git_branch}%B${user_symbol}%b "
RPROMPT="%B${return_code}%b"

ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg[yellow]%}("
ZSH_THEME_GIT_PROMPT_SUFFIX=") %{$reset_color%}"

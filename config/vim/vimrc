" Velleto ~/.vimrc

" Plugins {{{

" Install Plug if it isn't already. Makes vimrc more portable.
" Install Plug {{{
if !has('nvim') && empty(glob('~/.vim/autoload/plug.vim'))
    silent !echo "Plug Plugin manager isn't installed. Installing now. (vim)"
    silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    silent !echo ""
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif
if has('nvim') && empty(glob('~/.local/share/nvim/site/autoload/plug.vim'))
    silent !echo "Plug Plugin manager isn't installed. Installing now. (neovim)"
    silent !curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    silent !echo ""
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif
" " }}}
"
call plug#begin('~/.vim/bundle')
" Graphical undo tree visualisation.
Plug 'https://github.com/sjl/gundo.vim.git'
" Snippet expansion for syntax intensive languages.
" e.g. Defining the begin/end of TeX environments.
Plug 'https://github.com/SirVer/ultisnips'
" Distraction free editing.
Plug 'https://github.com/junegunn/goyo.vim'
" Make automatic asynchronous buils in background.
" e.g. Compiling TeX documents.
Plug 'https://github.com/tpope/vim-dispatch'
" Colourscheme.
Plug 'https://github.com/altercation/vim-colors-solarized'
" Add surrounding delimeters (e.g. ',",(),[],...) around a given string.
Plug 'https://github.com/tpope/vim-surround.git'
" Align text in columns by chars.
" e.g. Aligning '&' delimeters in equations or tables in TeX documents.
Plug 'https://github.com/junegunn/vim-easy-align'
" Extensive syntax and style checker.
Plug 'https://github.com/vim-syntastic/syntastic'
" Comment large sections of text in one command.
Plug 'https://github.com/tpope/vim-commentary.git'
" Show git status +,-,~ in gutter.
Plug 'https://github.com/airblade/vim-gitgutter.git'
" Move around text using visual markers.
Plug 'https://github.com/easymotion/vim-easymotion.git'
" Eye-candy tool for editiing multiple lines simultaneously.
Plug 'https://github.com/terryma/vim-multiple-cursors.git'
" Send R-language code to an interactive R shell by visual selection.
" Plug 'https://github.com/jalvesaq/Nvim-R'
" Show editing statistics of each file.
Plug 'https://github.com/mnishz/devotion.vim'
" BuildYCM {{{
function! BuildYCM(info)
  " Execute when YCM is installed or updated.
  if a:info.status == 'installed' || a:info.force
    !./install.py
  endif
endfunction
" }}}
" Live code completion.
Plug 'https://github.com/Valloric/YouCompleteMe', { 'do': function('BuildYCM') }
call plug#end()

" }}}
" Plugin Configurations {{{
" Syntastic
let g:syntastic_python_checkers = ['pycodestyle']

" goyo.
nnoremap Y :Goyo<CR>

" vim-easymotion.
" Mapleader must be set first.
let mapleader=" "
" Bi-Directional jump-by-Word invoked.
map <leader>w <Plug>(easymotion-bd-w)

" vim-easy-align.
nmap ga <Plug>(EasyAlign)
xmap ga <Plug>(EasyAlign)

" vim-commentary.
autocmd FileType matlab,octave setlocal commentstring=%\ %s

" vim-dispatch.
let b:pandoc_cmd = 'pandoc "' . expand("%") . '" -o "' . expand("%:r") . '.pdf"'
autocmd FileType markdown let b:dispatch = b:pandoc_cmd

" ultisnips.
set rtp+=$VIMSNIPPET
let g:UltiSnipsExpandTrigger="<c-h>"
let g:UltiSnipsJumpForwardTrigger="<c-j>"
let g:UltiSnipsJumpBackwardTrigger="<c-k>"
let g:UltiSnipsSnippetDirectories=[$VIMSNIPPET]
let g:UltiSnipsSnippetDir=$VIMSNIPPET
let g:UltiSnipsEditSplit="vertical"

" YCM.
let g:ycm_filetype_blacklist = {
      \ 'tagbar': 1,
      \ 'notes': 1,
      \ 'netrw': 1,
      \ 'unite': 1,
      \ 'text': 1,
      \ 'vimwiki': 1,
      \ 'infolog': 1,
      \ 'mail': 1
      \}
set completeopt-=preview
let g:ycm_min_num_of_chars_for_completion = 4

" vim-easy-align.
let g:easy_align_delimiters = {
  \ '%': {
  \       'pattern': '%\+',
  \       'delimiter_align': 'l',
  \       'ignore_groups': ['!Comment']
  \       },
  \ '\': {
  \       'pattern': '\\\+',
  \       },
  \ 't': {
  \       'pattern': '[&.*]*\\\\',
  \       },
  \ }
" }}}
" Swap, Backup, Undo and Spell files {{{
" Note: trailing slash to keep full paths in tmp files.
" Important if you have many files with the same name.
" WARNING: May create unencrypted undo-files of an encrypted file.
" Turn off persistent undo for these files using autocmds.

if empty(glob("~/.vim/undo"))
    silent !mkdir -p ~/.vim/undo
endif
if empty(glob("~/.vim/backup"))
    silent !mkdir -p ~/.vim/backup
endif
if empty(glob("~/.vim/swap"))
    silent !mkdir -p ~/.vim/swap
endif

set undodir=~/.vim/undo//
set backupdir=~/.vim/backup//
set directory=~/.vim/swap//

set undofile            " Save undos after file closes
set undolevels=1000     " How many undos
set undoreload=10000    " number of lines to save for undo

set spellfile=$VIMSPELL
" }}}
" General {{{
set nocompatible
filetype plugin on

syntax on
" Prevent vim from looking ahead more that `maxlines` for matching syntax.
syntax sync maxlines=100

" set background=light
colorscheme solarized

let mapleader=" "

set number
set relativenumber
set wrap                       " Soft wrap lines on screen.
set textwidth=80
set autoindent
set noerrorbells
set ttyfast
set lazyredraw
set showmatch                  " Highlight matching brackets.
set scrolloff=5                " Keep curser n lines from top/bottom.
set linebreak                  " Wrap at `breakat` var (not in middle of word).
set backspace=indent,eol,start " Allow 'backspacing' over any whitespace.
set updatetime=1000            " Time to wait (ms) before autosaving to swap.
set foldenable
set foldmethod=marker
set foldlevel=0
set foldnestmax=10
set conceallevel=0             " Don't show digrpaphs. 'Clever' feature.
set clipboard=unnamedplus
set encoding=utf-8

" WARNING: Some consider modelines to be a security threat.
set modeline                   " Set buffer specific vim settings in buffer.
set modelines=3                " Consier first/last n lines of buffer.

" Whitespace.
set tabstop=4                  " Number of visual spaces per TAB.
set softtabstop=4              " Number of spaces in tab when editing.
set expandtab                  " Convert tabs to spaces.
set shiftwidth=4               " Set indentation level.
set fillchars+=fold:\          " Fill folds with ' ', instead of '-'.

" Highlight trailing whitespace red.
highlight TrailingWhiteSpace ctermbg=red
match TrailingWhiteSpace /\s\+\%#\@<!$/

" Split directions.
set splitbelow
set splitright
set fillchars+=vert:\|
hi VertSplit ctermbg=NONE guibg=NONE

" Searching.
set incsearch                  " (Incremental) Search as characters are entered.
set ignorecase                 " Case insensitive searching.
set smartcase                  " Case sensistive if a capital letter is typed.
set hlsearch                   " Highlight search.
" }}}
" Status Line {{{
" Always show statusline.
set laststatus=2
" Don't additionally show mode underneath statusline.
set noshowmode

" Mode Dict {{{
let g:currentmode={
    \ 'n'      : 'NORMAL',
    \ 'no'     : 'N-PENDING',
    \ 'v'      : 'VISUAL',
    \ 'V'      : 'V-LINE',
    \ '\<C-V>' : 'V Block',
    \ 's'      : 'SELECT',
    \ 'S'      : 'S-LINE',
    \ '\<C-S>' : 'S-BLOCK',
    \ 'i'      : 'INSERT',
    \ 'R'      : 'REPLACE',
    \ 'Rv'     : 'V-REPLACE',
    \ 'c'      : 'COMMAND',
    \ 'cv'     : 'VIM-EX',
    \ 'ce'     : 'EX',
    \ 'r'      : 'PROMPT',
    \ 'rm'     : 'MORE',
    \ 'r?'     : 'CONFIRM',
    \ '!'      : 'SHELL',
    \ 't'      : 'TERMINAL'
    \}
" }}}
" (f) Git Info {{{
let b:git_info = ''

augroup GetGitBranch
  autocmd!
  autocmd BufEnter,BufWritePost * let b:git_info = GitInfo()
augroup END

function! GitInfo()

  let git = ''
  let git_cmd = 'git -C ' . expand('%:p:h')

  let branch_cmd = git_cmd . ' rev-parse --abbrev-ref HEAD 2> /dev/null'
  let branch = system(branch_cmd)

  " git puts strange characters on branch output. Sanitise output, remove
  " non-printable characters, i.e. those in ascii range ~ (tilde) to -
  " (minus).
  let branch = substitute(branch, '[^ -~]\+', '', '')

  " Get more information if in a git repo.
  if branch != ''
    let git .= '  '
    let git .= '(' . branch . ')'

    " Could be a new file in a git repo with no name yet.
    if expand('%:p') != ''
      let stats_cmd = git_cmd . ' diff --numstat ' . expand('%:p' )
      let stats = system(stats_cmd)

      " Could be a non-tracked file. Then there are no stats.
      if stats != ''
        " Parse.
        let stats_pattern = '^\([0-9]*\)\s*\([0-9]*\).*$'
        let stats = substitute(stats, stats_pattern, '\1(+) \2(-)', '')
        " Truncate.
        let stats = stats[0:14]
        let git .= ' ' . stats
      endif
    endif
  endif

  return git
endfunction
" }}}
" (f) Colours {{{
" 'bg' is text colour, 'fg' is the bar colour.
"
hi StatusLine ctermbg=010
hi StatusLine ctermfg=007

hi StatusLineExtra ctermbg=014
hi StatusLineExtra ctermfg=007

hi StatusLineMode ctermfg=015

" Automatically change the statusline color depending on mode.
function! ChangeStatuslineColor()
  if (mode() =~# '\v(n|no)')
    " Normal Mode.
    exe 'hi! StatusLineMode ctermbg=010'
  elseif (mode() =~# '\v(v|V)')
" Visual Mode.
    exe 'hi! StatusLineMode ctermbg=005'
  elseif (mode() ==# 'i')
" Insert Mode.
    exe 'hi! StatusLineMode ctermbg=004'
  else
" Other Mode.
    exe 'hi! StatusLineMode ctermbg=010'
  endif

  return ''
endfunction
" }}}
" (f) Buffer Statistics {{{
let b:buf_stats = ''

augroup GetFileStats
  autocmd!
  autocmd BufEnter,BufWritePost * let b:buf_stats = FileStats()
augroup END

function! FileStats()

  let stats = ''

  " If buffer hasn't been written yet, don't get size.
  if str2float(getfsize(expand('%:p'))) > 0

    " Buffer size.
    " let bytes = str2float(getfsize(expand('%:p')))

    " if bytes <= 0
    "   return '0'
    " endif

    " for size in ["B", "K", "M", "G"]
    "   if (abs(bytes) < 1000)
    "     return string(float2nr(round(bytes))) . size
    "   endif
    "   let bytes = bytes / 1000
    " endfor

    " Using system utils
    let size_cmd = 'du -sh ' . expand('%:p')
    let size = system(size_cmd)
    " Parse.
    let size_pattern = '^\(\S*\).*$'
    let size = substitute(size, size_pattern, '\1', '')

    " Word and character count.
    let counts_cmd = 'wc -mw ' . expand('%:p')
    let counts = system(counts_cmd)
    " Parse.
    let counts_pattern = '^\s*\([0-9]*\)\s*\([0-9]*\).*$'
    let counts = substitute(counts, counts_pattern, '\1(w) \2(c)', '')

    let stats = counts . ' ' . size

  endif

  return stats

endfunction
" }}}
" (f) Read Only {{{
let b:readonly_flag = ""

augroup ReadOnlyStatus
  autocmd!
  autocmd BufEnter,WinEnter * let b:readonly_flag = ReadOnly()
augroup END


function! ReadOnly()

  if &readonly || !&modifiable
    return "[READ ONLY]"
  else
    return ""

endfunction
" }}}
" (f) File Name {{{
" Change how much of the filename is displayed based on available terminal.
" space.

let b:filename = ""

augroup FileName
  autocmd!
  autocmd BufEnter,VimResized * let b:filename = FileName()
augroup END


function! FileName()

    let fullname = expand("%:p")

    if (fullname == "")
        return ' [New]'
    endif

    " See how much space is being occupied by the rest of the statusline
    " elements.
    let remainder =
          \ 25
          \ + len(&filetype)
          \ + len(&fenc)
          \ + len(g:currentmode[mode()])
          \ + len(get(b:, 'git_info', ''))
          \ + len(get(b:, 'readonly_flag', ''))
          \ + len(get(b:, 'buf_stats', ''))

    " If the full name doesn't fit, then use a shorter one.
    " Cases:
    " 1) Show full path.
    " 2) Show only first character of every directory in path.
    " 3) Show only basename.

    if (&columns > len(fullname) + remainder)

      let shortpath = substitute(fullname, $HOME, '~', "")
      return ' ' . shortpath

    elseif (&columns > len(pathshorten(fullname)) + remainder)

      let home = pathshorten($HOME . '/')
      let shortpath = substitute(pathshorten(fullname), home, '~/', "")
      return ' ' . shortpath

    else
        return ' ' . expand("%:t")
    endif

endfunction
" }}}

" Status Line Format String.
set statusline=
set statusline+=%{ChangeStatuslineColor()}
set statusline+=%#StatusLineMode#                               " Set colour.
set statusline+=\ %{g:currentmode[mode()]}                      " Get Mode.
set statusline+=\ %*                                            " Default colour.
set statusline+=%{get(b:,'filename','')}                        " Filename.
set statusline+=%{get(b:,'git_info','')}                        " Git branch.
set statusline+=%{get(b:,'readonly_flag','')}                   " Readonly flag.
set statusline+=\ %*                                            " Default colour.

set statusline+=\ %=                                            " Right Side.
set statusline+=\ %3(%{get(b:,'buf_stats','')}%)                " File size.
set statusline+=\ %#StatusLineExtra#                            " Set colour.
set statusline+=\ %<[%{substitute(&spelllang,'_..','','g')}] " Spell Language.
set statusline+=%{(&filetype!=''?'\ ['.&filetype.']':'')}       " FileType.
set statusline+=\ %#StatusLineMode#                             " Set colour.
set statusline+=\ %3p%%                                         " Percentage.
set statusline+=\ %3l/%-3L:%-3c\                                " Line/Column Numbers.

" }}}
" Tab Line {{{
" Only show tabline if there are two or more tabs.
set showtabline=1
set tabpagemax=5
set tabline=%!TabLine()

" Colours and Configurations {{{
let g:padding = 2
let g:mintablabellen = 5

hi TabLineSel ctermfg=007 cterm=None
hi TabLineSel ctermbg=010 cterm=None

hi TabLineNum ctermfg=015 cterm=None
hi TabLineNum ctermbg=014 cterm=None

hi TabLineBuffer ctermbg=014
hi TabLineBuffer ctermfg=007

hi TabLine cterm=None
hi TabLineFill cterm=None
" }}}
" (f) LabelName {{{
function! LabelName(n)
  " Decide label name for n'th buffer.

  let b:buftype = getbufvar(a:n, "&buftype")

  if b:buftype ==# 'help'
    " Show help page name and strip trailing '.txt'.
    let label = '[H] ' . fnamemodify(bufname(a:n), ':t:s/.txt$//')

  elseif b:buftype ==# 'quickfix'
    let label = '[Q]'

  elseif bufname(a:n) != ''
    " Only show basename (with extension) for regular files.
    let label = " " . fnamemodify(bufname(a:n), ':t')

  else
    let label = "[New]"

  endif

  return label

endfunction
" }}}
" (f) Maxlen {{{
function! MaxLen(iterable)
  let maxlen = 0
  for item in a:iterable
    if len(item) > maxlen
      let maxlen = len(item)
    endif
  endfor
  return maxlen
endfunction
" }}}
" (f) TabLine {{{
function! TabLine()
  " Set the format string for the tabline.

  " Explainer {{{
  " The main complexity in the following code arises due to the requirement to
  " have equally spaced tab labels. It means that we need to find what will be
  " visually displayed first (the chars printed to screen with spacing) to
  " find out how long each tab label will be and then go back over the buffer
  " list to add formatting strings (i.e. colour). If we did not do this, the
  " formatting strings would be included in the calculation of the length of
  " the tab labels, even though they don't take up any visual space.
  "
  " Example:
  " The format string
  "
  " '%1T%#TabLineSel#%#TabLineSel#    %#TabLineSel# vimrc %#TabLineBuffer# .tmux.conf     %2T%#TabLine#         [H] eval          %#TabLineFill#%T'
  "
  " takes up 141 chars, but will only display
  "
  " '     vimrc  .tmux.conf              [H] eval          '
  "
  " taking up 54 chars.
  " }}}
  " Get maximum length tab label {{{

  " Get all labels.
  let g:tablabels = []
  for t in range(tabpagenr('$'))
    let g:tablabel = ""
    for bufnum in tabpagebuflist(t + 1)
      let g:tablabel .= LabelName(bufnum)
    endfor
    call add(g:tablabels, g:tablabel)
  endfor

  " Find maximal length.
  " For equal with tabs, fitted to longest tab label.
  let g:maxlabellen = max([g:mintablabellen, MaxLen(g:tablabels)])
  " For full screen width equal width tabs.
  " let g:maxlabellen = &columns / tabpagenr('$')

  " }}}

  let tablfmt = ''

  " Iter over tabs.
  " Here we set the format strings, for correct colouring, we need to do it in
  " the following order:
  " '<colour><padding><buflabel>[[ <colour> <buflabel>]]*<padding>'

  for t in range(tabpagenr('$'))

    " New tab label begins here.
    let tablfmt .= '%' . (t+1) . 'T'

    " Set highlight.
    " #TabLineSel for selected tab, #TabLine otherwise.
    let tablfmt .= (t+1 == tabpagenr() ? '%#TabLineSel#' : '%#TabLine#')

    " Get buffer names and statuses.
    " Buffer names with formatting strings, colours etc.
    let t:labelfmt = ''
    " Number of buffers in tab.
    let t:bcount = len(tabpagebuflist(t+1))
    " Total amount of whitespace to fill, after considering curent tab label.
    let t:remainder = g:maxlabellen - len(g:tablabels[t])
    let t:pad = t:remainder /2 + g:padding

    " Iter over buffers in tab.
    for bnum in tabpagebuflist(t+1)

      " Set colour.
      if t+1 == tabpagenr()
        " If on current buffer in current tab, set bg colour dark.
        let bcolour = (bnum == bufnr("%") ? '%#TabLineSel#' : '%#TabLineBuffer#')
      else
        " If not on current tab, leave default bg colour.
        let bcolour = ''
      endif

      " Put padding before first buffer name in tab.
      if bnum == tabpagebuflist(t+1)[0]
        let t:labelfmt .= bcolour . repeat(" ", t:pad)
      endif

      let t:labelfmt .= bcolour . LabelName(bnum)

      " Don't add final space to buffer name.
      if t:bcount > 1
        let t:labelfmt .= ' '
      endif
      let t:bcount -= 1

    endfor

    let t:labelfmt .= repeat(" ", t:pad)
    let tablfmt .= t:labelfmt

  endfor

  " Fill to end.
  let tablfmt .= '%#TabLineFill#%T'

  return tablfmt
endfunction
" }}}
" }}}
" Mappings {{{
" Note: Don't add whitespace or anything after a *map command.

" How long to wait for a keysequence to be typed.
set timeout
set timeoutlen=200
set ttimeoutlen=1000

" Remap escape.
cnoremap jf <Esc>
cnoremap fj <Esc>
cnoremap jk <Esc>
cnoremap kj <Esc>
inoremap jf <Esc>
inoremap fj <Esc>
inoremap jk <Esc>
inoremap kj <Esc>

" Insert empty lines in normal mode.
nnoremap <CR> o<Esc>

" Move right in insert mode.
" inoremap <C-Space> <C-o>l

" Insert single char.
nnoremap = :exec "normal i".nr2char(getchar())."\e"<CR>

" Move vertically by visual line, not by file line.
" nnoremap j gj
" nnoremap k gk

" Show previous command.
noremap <C-P> :<C-P>

" Split navigations.
" nnoremap <C-J> <C-W><C-J>
" nnoremap <C-K> <C-W><C-K>
" nnoremap <C-L> <C-W><C-L>
" nnoremap <C-H> <C-W><C-H>

" Turn off search highlighting with space.
nnoremap <silent> <Space> :nohlsearch<Bar>:echo<CR>

" Buffer navigations.
nnoremap B :bnext<cr>
nnoremap H :bprevious<cr>

" Tab navigations.
nnoremap T :tabnext<cr>

" GNU-readline-like vim command line movements.
cnoremap <C-a> <Home>
cnoremap <C-e> <End>
cnoremap <C-p> <Up>
cnoremap <C-n> <Down>
cnoremap <C-b> <Left>
cnoremap <C-f> <Right>
cnoremap <C-d> <Delete>
" }}}
" Functions {{{
func! Time()
    " Write out Date.
    put =strftime('%Y-%m-%d %H:%M')
endfu

func! ShowDiff()
  if get(b:,'git_info','') != ''
    !git diff %
  else
    w !git diff -- % -
  endif
endfu

func! Speller()
    setlocal spell spelllang=en_gb,de_ch
    highlight SpellBad cterm=underline
endfu

func! SpellerD()
    setlocal spell spelllang=de_ch
    highlight SpellBad cterm=underline
endfu

func! SpellerE()
    setlocal spell spelllang=en_gb
    highlight SpellBad cterm=underline
endfu

func! MakePandoc()
    write
    execute '.!pandoc "' . expand("%") . '" -o "' . expand("%:r") . '.pdf &"'
endfu

func! ShowPDF()
    execute '!open "' . expand("%:r") . '.pdf"'
endfu

let g:rules = 0
func! GuideRules()
    if g:rules
        highlight ColorColumn ctermbg=015
        let g:rules = 0
    else
        let &colorcolumn="80,".join(range(120,999),",")
        highlight ColorColumn ctermbg=007
        let g:rules = 1
    endif
endfu

com! S  call Speller()
com! Se call SpellerE()
com! Sd call SpellerD()
com! G  call GuideRules()
com! D  call ShowDiff()
com! Ps call MakePandoc()
com! V  call ShowPDF()
com! T  call Time()

" Common typos.
com! W w
com! Q q
" }}}
" Exceptions {{{
augroup vim
  autocmd!
  autocmd FileType vim setlocal
    \ tabstop=2
    \ softtabstop=2
    \ shiftwidth=2
augroup END

augroup Markdown
  autocmd!
  autocmd FileType markdown setlocal
    \ commentstring=<!--\ %s\ -->
    \ conceallevel=0
    \ foldmethod=indent
    \ shiftwidth=2
    \ softtabstop=2
    \ spell spelllang=de_ch,en_gb
    \ syntax=off
    \ tabstop=2
    \ textwidth=80
augroup END

augroup Python
  autocmd!
  autocmd FileType python call GuideRules()
augroup END

augroup EmailEdit
  autocmd!
  " Turn off numberings in mutt.
  autocmd BufEnter /tmp/*mutt*,/private*mutt* setlocal
    \ nonumber
    \ norelativenumber
    \ shiftwidth=4
    \ showtabline=1
    \ spell spelllang=de_ch,en_gb
  autocmd BufEnter /tmp/*mutt*,/private*mutt* highlight SpellBad cterm=underline
augroup END

augroup TexHighlight
  autocmd!
  autocmd FileType tex setlocal
    \ conceallevel=0
    \ shiftwidth=2
    \ softtabstop=2
    \ spell spelllang=de_ch,en_gb
    \ tabstop=2
    \ textwidth=80
  autocmd FileType tex highlight SpellBad cterm=underline
augroup END

augroup SecretEdit
  autocmd!
  " Turn off persistant undo for encrypted files.
  " pass uses /dev/shm/pass.*
  autocmd BufEnter /dev/shm/* setlocal noundofile
augroup END
" }}}

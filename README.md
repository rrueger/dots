# Dotfiles

After cloning this project

```
git submodule update --init --recursive
```

to get dependencies. Then copy

```
cp zshrc.example ~/.zshrc
cp vimrc.example ~/.vimrc
```

and follow the instructions. Finally symlink the `tmux.conf`

```
ln -s tmux.conf ~/.tmux.conf
```

These dotfiles can be modularly placed along side existing configurations. Can
even be added as a submodule to existing configuration repositories.
